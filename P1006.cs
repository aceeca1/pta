using System;

class P1006 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var n1 = n % 10;
        var n2 = n / 10 % 10;
        var n3 = n / 100;
        var s1 = new string('B', n3);
        var s2 = new string('S', n2);
        var s3 = "123456789".Substring(0, n1);
        Console.WriteLine("{0}{1}{2}", s1, s2, s3);
    }
}
