using System;

class P1011 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a = long.Parse(line[0]);
            var b = long.Parse(line[1]);
            var c = long.Parse(line[2]);
            var answer = a + b > c ? "true" : "false";
            Console.WriteLine("Case #{0}: {1}", i + 1, answer);
        }
    }
}
