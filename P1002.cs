using System;
using System.Linq;

class P1002 {
    static string[] Pinyin = {
        "ling", "yi",  "er", "san", "si", 
        "wu",   "liu", "qi", "ba",  "jiu"
    };

    public static void Main() {
        var s = Console.ReadLine().Sum(k => k - '0').ToString();
        Console.WriteLine(string.Join(" ", s.Select(k => Pinyin[k - '0'])));
    }
}
