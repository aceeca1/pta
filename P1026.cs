using System;

class P1026 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var c1 = int.Parse(line[0]);
        var c2 = int.Parse(line[1]);
        var second = (c2 - c1 + 50) / 100;
        var minute = second / 60;
        second %= 60;
        var hour = minute / 60;
        minute %= 60;
        Console.WriteLine("{0:D2}:{1:D2}:{2:D2}", hour, minute, second);
    }
}
