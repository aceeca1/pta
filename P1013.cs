using System;
using System.Collections.Generic;
using System.Linq;

class P1013 {
    static IEnumerable<int> AllPrime(int n) {
        var composite = new bool[n + 1];
        for (long i = 2; i <= n; ++i) {
            if (!composite[i]) yield return (int)i;
            for (long j = i * i; j <= n; j += i) {
                composite[j] = true;
            }
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var m = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        var answer = AllPrime(200000).Skip(m - 1).Take(n - m + 1);
        var currentLine = new List<int>();
        foreach (var i in answer) {
            currentLine.Add(i);
            if (currentLine.Count == 10) {
                Console.WriteLine(string.Join(" ", currentLine));
                currentLine.Clear();
            }
        }
        if (currentLine.Count != 0) {
            Console.WriteLine(string.Join(" ", currentLine));
        }
    }
}
