using System;

class P1009 {
    public static void Main() {
        var a = Console.ReadLine().Split();
        Array.Reverse(a);
        Console.WriteLine(string.Join(" ", a));
    }
}
