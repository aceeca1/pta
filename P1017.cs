using System;

class Divide {
    public string Div;
    public int Mod = 0;

    public Divide(string a1, int a2) {
        var answer = new char[a1.Length];
        for (int i = 0; i < a1.Length; ++i) {
            Mod = a1[i] - '0' + Mod * 10;
            answer[i] = (char)('0' + Mod / a2);
            Mod %= a2;
        }
        if (1 < answer.Length && answer[0] == '0') {
            Div = new string(answer, 1, answer.Length - 1);
        } else {
            Div = new string(answer);
        }
    }
}

class P1017 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var divide = new Divide(line[0], int.Parse(line[1]));
        Console.WriteLine("{0} {1}", divide.Div, divide.Mod);
    }
}
