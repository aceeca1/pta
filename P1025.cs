using System;
using System.Collections.Generic;

class Node {
    public int Address, Data, Next;
}

class P1025 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var head = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        var k = int.Parse(line[2]);
        var a = new Dictionary<int, Node>();
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var address = int.Parse(line[0]);
            a[address] = new Node {
                Address = address,
                Data = int.Parse(line[1]),
                Next = int.Parse(line[2])
            };
        }
        var answer = new Node[n];
        for (int i = 0; i < n; ++i) {
            answer[i] = a[head];
            head = a[head].Next;
        }
        for (int i = 0; i <= n - k; i += k) Array.Reverse(answer, i, k);
        for (int i = 0; i < n - 1; ++i) {
            Console.WriteLine("{0:D5} {1} {2:D5}",
                answer[i].Address, answer[i].Data, answer[i + 1].Address);
        }
        Console.WriteLine("{0:D5} {1} -1",
            answer[n - 1].Address, answer[n - 1].Data);
    }
}
