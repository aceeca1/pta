using System;
using System.Collections.Generic;
using System.Linq;

class P1012 {
    static int AltSum(List<int> a) {
        var negative = false;
        var answer = 0;
        foreach (var i in a) {
            answer += negative ? -i : i;
            negative = !negative;
        }
        return answer;
    }

    static IEnumerable<string> Solve(int[] a) {
        var b = new List<int>[5];
        for (int i = 0; i < 5; ++i) b[i] = new List<int>();
        foreach (var i in a.Skip(1)) b[i % 5].Add(i);
        yield return b[0].Count(k => k % 2 == 0) == 0 ? "N" : 
            b[0].Where(k => k % 2 == 0).Sum().ToString();
        yield return b[1].Count == 0 ? "N" : AltSum(b[1]).ToString();
        yield return b[2].Count == 0 ? "N" : b[2].Count.ToString();
        yield return b[3].Count == 0 ? "N" : b[3].Average().ToString("F1");
        yield return b[4].Count == 0 ? "N" : b[4].Max().ToString();
    }


    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(string.Join(" ", Solve(a)));
    }
}
