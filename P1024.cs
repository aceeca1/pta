using System;

class P1024 {
    static string Unscientific(string a) {
        var sign = a[0] == '-' ? "-" : "";
        var split = a.Substring(1).Split('E');
        var coefficient = split[0];
        var exponent = int.Parse(split[1]);
        if (exponent < 0) {
            var part1 = "0.";
            var part2 = new string('0', - 1 - exponent);
            var part3 = coefficient[0];
            var part4 = coefficient.Substring(2);
            return String.Concat(sign, part1, part2, part3, part4);
        } else if (exponent < coefficient.Length - 2) {
            var part1 = coefficient[0];
            var part2 = coefficient.Substring(2, exponent);
            var part3 = '.';
            var part4 = coefficient.Substring(2 + exponent);
            return String.Concat(sign, part1, part2, part3, part4);
        } else {
            var part1 = coefficient[0];
            var part2 = coefficient.Substring(2);
            var part3 = new string('0', exponent - (coefficient.Length - 2));
            return String.Concat(sign, part1, part2, part3);
        }
    }

    public static void Main() {
        var a = Console.ReadLine();
        Console.WriteLine(Unscientific(a));
    }
}
