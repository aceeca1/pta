using System;
using System.Collections.Generic;

class Mooncake {
    public double Amount, Price;
}

class PriceDecrease : IComparer<Mooncake> {
    public int Compare(Mooncake m1, Mooncake m2) {
        return m2.Price.CompareTo(m1.Price);
    }
}

class P1020 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var d = double.Parse(line[1]);
        var mooncake = new Mooncake[n];
        for (int i = 0; i < n; ++i) mooncake[i] = new Mooncake();
        line = Console.ReadLine().Split();
        for (int i = 0; i < n; ++i) {
            mooncake[i].Amount = double.Parse(line[i]);
        }
        line = Console.ReadLine().Split();
        for (int i = 0; i < n; ++i) {
            mooncake[i].Price = double.Parse(line[i]) / mooncake[i].Amount;
        }
        Array.Sort(mooncake, new PriceDecrease());
        var total = 0.0;
        foreach (var i in mooncake) {
            if (d <= i.Amount) {
                total += i.Price * d;
                break;
            } else {
                total += i.Price * i.Amount;
                d -= i.Amount;
            }
        }
        Console.WriteLine("{0:F2}", total);
    }
}
