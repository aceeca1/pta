using System;
using System.Linq;

class P1018 {
    static int Compare(char a1, char a2) {
        if (a1 == a2) return 0;
        if (a1 == 'C') return a2 == 'J' ? 1 : -1;
        if (a1 == 'J') return a2 == 'B' ? 1 : -1;
        if (a1 == 'B') return a2 == 'C' ? 1 : -1;
        return 0;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var times = new int[3];
        var winA = new int[26];
        var winB = new int[26];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine();
            var compare = Compare(line[0], line[2]);
            ++times[compare + 1];
            if (compare ==  1) ++winA[line[0] - 'A'];
            if (compare == -1) ++winB[line[2] - 'A'];
        }
        Console.WriteLine("{0} {1} {2}", times[2], times[1], times[0]);
        Console.WriteLine("{0} {1} {2}", times[0], times[1], times[2]);
        var bestA = (char)('A' + Array.IndexOf(winA, winA.Max()));
        if (bestA == 'A') bestA = 'B';
        var bestB = (char)('A' + Array.IndexOf(winB, winB.Max()));
        if (bestB == 'A') bestB = 'B';
        Console.WriteLine("{0} {1}", bestA, bestB);
    }
}
