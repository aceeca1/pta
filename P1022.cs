using System;

class P1022 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var d = int.Parse(line[2]);
        Console.WriteLine(Convert.ToString(a + b, d));
    }
}
