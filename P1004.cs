using System;
using System.Linq;

class Student {
    public string Name, Id;
    public int Score;
}

class P1004 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var student = new Student[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            student[i] = new Student {
                Name = line[0],
                Id = line[1],
                Score = int.Parse(line[2])
            };
        }
        var maxScore = student.Max(k => k.Score);
        var minScore = student.Min(k => k.Score);
        var argmax = student.First(k => k.Score == maxScore);
        var argmin = student.First(k => k.Score == minScore);
        Console.WriteLine("{0} {1}", argmax.Name, argmax.Id);
        Console.WriteLine("{0} {1}", argmin.Name, argmin.Id);
    }
}
