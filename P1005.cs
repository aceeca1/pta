using System;
using System.Collections.Generic;

class P1005 {
    static int F(int n) {
        return n % 2 == 0 ? n / 2 : (n * 3 + 1) / 2;
    }

    static IEnumerable<int> ImportantNumbers(int[] a) {
        var visited = new HashSet<int>();
        foreach (var i in a) {
            var number = i;
            while (number != 1) {
                number = F(number);
                visited.Add(number);
            }
        }
        for (int i = a.Length - 1; i >= 0; --i) {
            if (!visited.Contains(a[i])) yield return a[i];
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        Console.WriteLine(string.Join(" ", ImportantNumbers(a)));
    }
}
