using System;
using System.Collections.Generic;
using System.Linq;

class P1007 {
    static IEnumerable<int> AllPrime(int n) {
        var composite = new bool[n + 1];
        for (long i = 2; i <= n; ++i) {
            if (!composite[i]) yield return (int)i;
            for (long j = i * i; j <= n; j += i) {
                composite[j] = true;
            }
        }
    }

    static IEnumerable<int> PrimePair(int n) {
        var lastPrime = 1;
        foreach (var i in AllPrime(n)) {
            if (i - lastPrime == 2) yield return i;
            lastPrime = i;
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(PrimePair(n).Count());
    }
}
