using System;
using System.Collections.Generic;

class P1010 {
    static IEnumerable<int> D(int[] a) {
        var isZero = true;
        for (int i = 0; i < a.Length; i += 2) {
            var c = a[i] * a[i + 1];
            var p = a[i + 1] - 1;
            if (c != 0) {
                isZero = false;
                yield return c;
                yield return p;
            }
        }
        if (isZero) {
            yield return 0;
            yield return 0;
        }
    }

    public static void Main() {
        var option = StringSplitOptions.RemoveEmptyEntries;
        var line = Console.ReadLine().Split(null as char[], option);
        var a = Array.ConvertAll(line, int.Parse);
        Console.WriteLine(string.Join(" ", D(a)));
    }
}
