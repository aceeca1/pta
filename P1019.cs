using System;

class P1019 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        if (n % 1111 == 0) {
            Console.WriteLine("{0} - {0} = 0000", n);
        } else {
            while (true) {
                var s = n.ToString("D4").ToCharArray();
                Array.Sort(s);
                var s1 = new string(s);
                Array.Reverse(s);
                var s2 = new string(s);
                var difference = int.Parse(s2) - int.Parse(s1);
                Console.WriteLine("{0} - {1} = {2:D4}", s2, s1, difference);
                if (difference == 6174) break;
                n = difference;
            }
        }
    }
}
