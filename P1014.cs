using System;

class Time {
    static string[] DayName = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
    public string Day;
    public int Hour, Minute;

    public Time(string s1, string s2, string s3, string s4) {
        int k;
        for (k = 0; k < s1.Length; ++k) {
            if (s1[k] == s2[k] && 'A' <= s1[k] && s1[k] <= 'G') {
                Day = DayName[s1[k] - 'A'];
                break;
            }
        }
        for (k = k + 1; k < s1.Length; ++k) {
            if (s1[k] == s2[k] && 'A' <= s1[k] && s1[k] <= 'N') {
                Hour = s1[k] - 'A' + 10;
                break;
            }
            if (s1[k] == s2[k] && '0' <= s1[k] && s1[k] <= '9') {
                Hour = s1[k] - '0';
                break;
            }
        }
        for (k = 0; k < s3.Length; ++k) {
            if (s3[k] == s4[k] && char.IsLetter(s3[k])) {
                Minute = k;
                break;
            }
        }
    }
}

class P1014 {

    public static void Main() {
        var s1 = Console.ReadLine();
        var s2 = Console.ReadLine();
        var s3 = Console.ReadLine();
        var s4 = Console.ReadLine();
        var time = new Time(s1, s2, s3, s4);
        Console.WriteLine(
            "{0} {1:D2}:{2:D2}", 
            time.Day, time.Hour, time.Minute);
    }
}
