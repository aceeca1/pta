using System;
using System.Collections.Generic;

class P1023 {
    static IEnumerable<string> MinNumber(int[] times) {
        var first = Array.FindIndex(times, 1, k => k != 0);
        --times[first];
        yield return first.ToString();
        for (int i = 0; i < 10; ++i) {
            yield return new string((char)('0' + i), times[i]);
        }
    }

    public static void Main() {
        var times = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(string.Concat(MinNumber(times)));
    }
}
