using System;
using System.Linq;

class P1003 {
    static bool Accept(string s) {
        var numA = s.Count(k => k == 'A');
        var numP = s.Count(k => k == 'P');
        var numT = s.Count(k => k == 'T');
        if (numA + numP + numT != s.Length) return false;
        if (numP != 1 || numT != 1) return false;
        var indexP = s.TakeWhile(k => k != 'P').Count();
        var indexT = s.TakeWhile(k => k != 'T').Count();
        var a1 = indexP;
        var a2 = indexT - indexP - 1;
        var a3 = s.Length - indexT - 1;
        return a2 != 0 && a1 * a2 == a3;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            Console.WriteLine(Accept(s) ? "YES" : "NO");
        }
    }
}
