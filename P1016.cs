using System;
using System.Linq;

class P1016 {
    static int P(string a, char d) {
        var answer = string.Concat(a.Where(k => k == d));
        return answer.Length == 0 ? 0 : int.Parse(answer);
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var pA = P(line[0], line[1][0]);
        var pB = P(line[2], line[3][0]);
        Console.WriteLine(pA + pB);
    }
}
