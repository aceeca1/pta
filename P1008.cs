using System;
using System.Collections.Generic;

class P1008 {
    static IEnumerable<int> ShiftRight(int[] a, int m) {
        for (int i = a.Length - m; i < a.Length; ++i) {
            yield return a[i];
        }
        for (int i = 0; i < a.Length - m; ++i) {
            yield return a[i];
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(string.Join(" ", ShiftRight(a, m % n)));
    }
}
