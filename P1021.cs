using System;

class P1021 {
    public static void Main() {
        var times = new int[10];
        foreach (var i in Console.ReadLine()) ++times[i - '0'];
        for (int i = 0; i < 10; ++i) {
            if (times[i] == 0) continue;
            Console.WriteLine("{0}:{1}", i, times[i]);
        }
    }
}
