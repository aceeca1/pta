using System;
using System.Collections.Generic;
using System.Linq;

class P1001 {
    static long F(long n) {
        return n % 2 == 0 ? n / 2 : (n * 3 + 1) / 2;
    }

    static IEnumerable<long> Callatz(long n) {
        while (n != 1) {
            yield return n;
            n = F(n);
        }
    }

    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        Console.WriteLine(Callatz(n).Count());
    }
}
