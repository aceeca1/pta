using System;
using System.Collections.Generic;

class Student {
    public string Id;
    public int Moral, Ability;
}

class Judger : IComparer<Student> {
    public int H;

    int WhichClass(Student student) {
        if (H <= student.Moral && H <= student.Ability) return 1;
        if (H <= student.Moral) return 2;
        if (student.Ability < H && student.Ability <= student.Moral) return 3;
        return 4;
    }

    int Total(Student student) {
        return student.Moral + student.Ability;
    }

    public int Compare(Student s1, Student s2) {
        var class1 = WhichClass(s1);
        var class2 = WhichClass(s2);
        if (class1 < class2) return -1;
        if (class2 < class1) return 1;
        var total1 = Total(s1);
        var total2 = Total(s2);
        if (total1 < total2) return 1;
        if (total2 < total1) return -1;
        if (s1.Moral < s2.Moral) return 1;
        if (s2.Moral < s1.Moral) return -1;
        return s1.Id.CompareTo(s2.Id);
    }
}

class P1015 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var l = int.Parse(line[1]);
        var h = int.Parse(line[2]);
        var student = new Student[n];
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            student[i] = new Student {
                Id = line[0],
                Moral = int.Parse(line[1]),
                Ability = int.Parse(line[2])
            };
        }
        student = Array.FindAll(student, k => l <= k.Moral && l <= k.Ability);
        Array.Sort(student, new Judger{H = h});
        Console.WriteLine(student.Length);
        foreach (var i in student) {
            Console.WriteLine("{0} {1} {2}", i.Id, i.Moral, i.Ability);
        }
    }
}
